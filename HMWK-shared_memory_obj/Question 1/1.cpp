#include<boost/interprocess/managed_shared_memory.hpp>
#include<boost/interprocess/mapped_region.hpp>
#include<iostream>

using namespace boost::interprocess;
using namespace std;

void main()
{
	//remove the preceding unknown version of the shared memory object
	shared_memory_object::remove("wzq");
	//create the object
	shared_memory_object shm_obj(create_only,"wzq",read_write);

	//set the size
	shm_obj.truncate(4);
	//map the whole shared memory in this process
	mapped_region region(shm_obj, read_write);

	int res = 123;
	int *tool = static_cast<int*>(region.get_address());
	*tool = res;

	//see the initialized value
	cout << "Initialized: " << *tool << endl;
}