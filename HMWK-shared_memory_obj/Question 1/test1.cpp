#include<boost/interprocess/managed_shared_memory.hpp>
#include<boost/interprocess/mapped_region.hpp>
#include<iostream>

using namespace boost::interprocess;
using namespace std;

void main()
{

	//open the shared_memory_object
	shared_memory_object shm(open_only, "wzq", read_write);
	//map the whole shared memory in this process
	mapped_region region(shm, read_write);

	int *tool = static_cast<int*>(region.get_address());
	cout << "In client before initialization again: " << *tool << endl;


	//modify the shared_memory_object in the new program
	int num;
	cout << "Please enter any number you like:\n";
	cin >> num;
	*tool = num;
	cout << "In client after initialization again: " << *tool << endl;

	//remove the shared memory after the test program is over
	shared_memory_object::remove("wzq");
}